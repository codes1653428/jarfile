# Use the official OpenJDK 11 as the base image
FROM openjdk:11

# Copy the JAR file into the Docker image
COPY g.jar /app/g.jar
COPY g.txt /app/g.txt

# Set the working directory to /app
WORKDIR /app

# Define the command to run your JAR file with the desired parameters
CMD ["java", "-jar", "g.jar", "-s", "14.113.1.124:30999", "-p", "BOT_", "-d", "1", "3", "-x", "-c", "9999", "-t", "SOCKS4", "-l", "g.txt"]
